import React from 'react';
import ReactDOM from 'react-dom';
import { render, cleanup } from '@testing-library/react';
import App from './App';
import Input from './Input';
import {shallow} from 'enzyme';

afterEach(cleanup);

const setup = () => {
  const utils = render(<Input />)
  const input = utils.getByLabelText('Test:')
  return {
    input,
    ...utils,
  }
}

test('renders without crashing', () => {
  const div = document.createElement('div');
  ReactDOM.render(<App />, div);
});


test("F", () => {
  const component = shallow(<Input />);

  component
    .find('#input')
    .simulate("change", { target: { value: "F" } });

  expect(component.state("yPosition")).toEqual(1);
  expect(component.state("xPosition")).toEqual(0);
  expect(component.state("direction")).toEqual(0);
});

test("FF", () => {
  const component = shallow(<Input />);

  component
    .find('#input')
    .simulate("change", { target: { value: "FF" } });

  expect(component.state("yPosition")).toEqual(2);
  expect(component.state("xPosition")).toEqual(0);
  expect(component.state("direction")).toEqual(0);
});

test("R", () => {
  const component = shallow(<Input />);

  component
    .find('#input')
    .simulate("change", { target: { value: 'R' } });

  expect(component.state("direction")).toEqual(1);
  expect(component.state("xPosition")).toEqual(0);
  expect(component.state("yPosition")).toEqual(0);
});

test("RR", () => {
  const component = shallow(<Input />);

  component
    .find('#input')
    .simulate("change", { target: { value: 'RR' } });

  expect(component.state("direction")).toEqual(2);
  expect(component.state("xPosition")).toEqual(0);
  expect(component.state("yPosition")).toEqual(0);
});


test("L", () => {
  const component = shallow(<Input />);

  component
    .find('#input')
    .simulate("change", { target: { value: 'L' } });

  expect(component.state("direction")).toEqual(-1);
  expect(component.state("xPosition")).toEqual(0);
  expect(component.state("yPosition")).toEqual(0);
});

test("LL", () => {
  const component = shallow(<Input />);

  component
    .find('#input')
    .simulate("change", { target: { value: 'LL' } });

  expect(component.state("direction")).toEqual(-2);
  expect(component.state("xPosition")).toEqual(0);
  expect(component.state("yPosition")).toEqual(0);
});

test("RRRRR", () => {
  const component = shallow(<Input />);

  component
    .find('#input')
    .simulate("change", { target: { value: 'RRRRR' } });

  expect(component.state("direction")).toEqual(5);
  expect(component.state("xPosition")).toEqual(0);
  expect(component.state("yPosition")).toEqual(0);
});

test("RF", () => {
  const component = shallow(<Input />);

  component
    .find('#input')
    .simulate("change", { target: { value: 'RF' } });

  expect(component.state("direction")).toEqual(1);
  expect(component.state("xPosition")).toEqual(1);
  expect(component.state("yPosition")).toEqual(0);
});

test("LF", () => {
  const component = shallow(<Input />);

  component
    .find('#input')
    .simulate("change", { target: { value: 'LF' } });

  expect(component.state("direction")).toEqual(-1);
  expect(component.state("xPosition")).toEqual(-1);
  expect(component.state("yPosition")).toEqual(0);
});


test("RRRRRRRF", () => {
  const component = shallow(<Input />);

  component
    .find('#input')
    .simulate("change", { target: { value: 'RRRRRRRF' } });

  expect(component.state("direction")).toEqual(7);
  expect(component.state("xPosition")).toEqual(-1);
  expect(component.state("yPosition")).toEqual(0);
});



