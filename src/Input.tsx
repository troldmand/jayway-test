import * as React from 'react';
import Board from './Board';

export default class Input extends React.Component<any, any> {
    constructor(props: any){
        super(props);

        this.state = {
            yPosition: 0,
            xPosition: 0,
            startX: 0,
            startY: 0,
            Direction: 0,
            startDirection: 0,
            report: "0 0 N",
        }
    }

    public handleOnChangeInput(event: any) { // not good typescript, use SyntheticEvent<T> interface
        let xPosition:number = this.state.startX;
        let yPosition:number = this.state.startY;
        let startX:number = this.state.startX;
        let startY:number = this.state.startY;
        let direction:number = this.state.startDirection;
        let startDirection:number = this.state.startDirection;
        let directionLetter:string = "";
        
        // This is capturing the input value ...
        var input:string = event.target.value;
        // ... and 2nd line is filtering anything but R, L or F away.
        var output:any = input.replace( /(l|L|r|R|f|F)|[^]/g, '$1').toUpperCase();
        // ... then we list it, for the switch-case
        let instructions = [...output];

        instructions.forEach((character) => { 
            switch (character) {
                case "F":
                    if (direction % 4 === -3) {
                    this.setState({xPosition: xPosition--})
                    }
                    if (direction % 4 === -2) {
                    this.setState({yPosition: yPosition--})
                    }
                    if (direction % 4 === -1) {
                    this.setState({xPosition: xPosition--})
                    }
                    if (direction % 4 === 0) {
                    this.setState({yPosition: yPosition++})
                    }
                    if (direction % 4 === 1) {
                    this.setState({xPosition: xPosition++})
                    }
                    if (direction % 4 === 2) {
                    this.setState({yPosition: yPosition--})
                    }
                    if (direction % 4 === 3) {
                    this.setState({xPosition: xPosition--})
                    }
                    break;
                case "R":
                    this.setState({direction: direction++});
                    
                    break;
                case "L":
                    this.setState({direction: direction--});
                    break;
                default:
                    break;
            };
        });
        
        if (direction % 4 === -3) {
            directionLetter = "E"
        }

        if (direction % 4 === -2) {
            directionLetter = "S"
        }

        if (direction % 4 === -1) {
            directionLetter = "W"
        }

        if (direction % 4 === 0) {
            directionLetter = "N"
        }

        if (direction % 4 === 1) {
            directionLetter = "E"
        }

        if (direction % 4 === 2) {
            directionLetter = "S"
        }

        if (direction % 4 === 3) {
            directionLetter = "W"
        }

        let report:string = xPosition+" "+yPosition+" "+directionLetter
        
        this.setState({ 
            output: output, 
            input: input, 
           
            yPosition: yPosition, 
            xPosition: xPosition, 
            direction: direction,
           
            startDirection: startDirection,
            startX: startX,
            startY: startY,
           
            report: report,
        });
    }

    public handleOnChangeX(event: any) : void {
        
      // This is capturing the input value ...
      var input:string = event.target.value;
      // ... and 2nd line is filtering anything but R, L or F away.
      var output:any = input.replace( /(1|2|3|4|5|6|7|8|9|0)|[^]/g, '$1');
      let startX:number = output;

      this.setState({startX: startX});

    }

    public handleOnChangeY(event: any) : void {
        
      // This is capturing the input value ...
      var input:string = event.target.value;
      // ... and 2nd line is filtering anything but R, L or F away.
      var output:any = input.replace( /(1|2|3|4|5|6|7|8|9|0)|[^]/g, '$1');
      let startY:number = output;

      this.setState({startY: startY});
    }

    public handleOnChangeDirection(event: any) : void {
      // This is capturing the input value ...
      var input:string = event.target.value;
      // ... and 2nd line is filtering anything but characters for north, east, west and south away.
      var output:any = input.replace( /(n|N|e|E|s|S|w|W)|[^]/g, '$1').toUpperCase();
      let startDirection:number = 0;
      let chars = [...output];

      chars.forEach((c) => { 
          switch (c) {
            case "E":
              this.setState({startDirection: startDirection++});
              break;
            case "S":
              this.setState({startDirection: startDirection++});
              this.setState({startDirection: startDirection++}); // 2nd time for easy increment method (built in)
              break;
            case "W":
              this.setState({startDirection: startDirection--});
              break;

            case "N": // doing nothing, because it's default
              break;

            default:
              break;
          };
      });

      this.setState({  
          startDirection: startDirection
      });

    }

    public render() {
        return (
            <div>
              <div>
                <input 
                  onChange={ e => this.handleOnChangeX(e) }
                  placeholder="X"
                  type="number"
                  className="smallInput"
                />
                <input 
                  onChange={ e => this.handleOnChangeY(e) }
                  placeholder="Y"
                  type="number"
                  className="smallInput"
                />
                <input 
                  onChange={ e => this.handleOnChangeDirection(e) }
                  placeholder="Ex. E"
                  id="DIR" // for test
                  type="text"
                  className="smallInput"
                />
              </div>
              <div>
                <input 
                  onChange={ e => this.handleOnChangeInput(e) }
                  placeholder="INPUT"
                  id="input" // for test
                  type="text"
                />
              </div>
              <br />
              <div>
                { this.state.report }
              </div>
              <br />
              <div>
                <Board report={ this.state.report }/>
              </div>
            </div>
        );
    }
}