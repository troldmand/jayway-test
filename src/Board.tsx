import * as React from 'react';

export default class Board extends React.Component<any, any> {
    constructor(props: any){
      super(props);

      this.state = {
        boardSize: 5
      };
    }

    renderSquare(i:string) {
      
      let report = this.props.report.slice(0, -2);
      let position = i;
      const figure:string = "♚";

      if (report === position) { // Conditional; if 'report' is 'position', then set King
        return <Square active={ figure } key={ i.toString() } value={ i.toString() } />;
      } else {
        return <Square key={ i.toString() } value={ i.toString() } />;
      }
    }

    lessSquares(e:any, boardSize:number) {
      e.preventDefault();
      this.setState({ 
        boardSize: boardSize--
      });
      this.setState({ 
        boardSize: boardSize,
      });
    };

    moreSquares(e:any, boardSize:number) {
      e.preventDefault();
      this.setState({ 
        boardSize: boardSize++
      });
      this.setState({ 
        boardSize: boardSize,
      });
    };

    render() {
        let boardSize:number = this.state.boardSize;
        let position:string = "";
        let squares:any = [];

        for(let count=boardSize; count > 0; count--) {
          let row = [];

          for(let j=0; j < boardSize; j++) {
            position = (j)+" "+(count-1); // '1,2 ex'
            row.push(this.renderSquare(position));
          }

          squares.push(<div key={ count.toString() } className="board-row">{row}</div>);
        }

        return (
          <div>
            <div>{ squares }</div>
            <p>{ this.state.report }</p>
            <button onClick={e => this.lessSquares(e, boardSize)}>Less</button>
            <button onClick={e => this.moreSquares(e, boardSize)}>More</button>  
          </div>
        );
      }
}

class Square extends React.Component<any, any> {

  render() {
    return (
      <button 
        id={ this.props.value.toString() }
        className="square">
        { this.props.active }
      </button>
    );
  }
}
